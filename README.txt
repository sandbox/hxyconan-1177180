
Taxonomy Term Navigation - README

DESCRIPTION
-----------

Taxonomy Term Navigation creates a block containing a previous and a next navigation links between node pages tagged with same taxonomy term.

This version only would effect the node pages with one taxonomy term attached.

REQUIREMENTS
------------

 * Taxonomy module
 * For updating navigation links scheduled, cron is required.

INSTALLATION
------------

 * Create a new directory "term_nav" in the sites/all/modules directory and place the entire contents of this folder in it.
 * Enable the module on the Modules admin page: Administer > Site building > Modules
 * Grant the access at the Access control page: Administer > User management > Access control.
 * Enable the "Taxonomy Term Link" block in block list page to the region you want to place the link
 * See http://drupal.org/node/70151 for further information about installation.
 
Configuration
------------

 * Browse to admin/settings/term_nav
 * Click the "Regenerate all navigation arraies" button in admin/settings/term_nav configuration page to generate links up-to-date, after finishing creating new or editing existing note pages with their taxonomy term.  
 

